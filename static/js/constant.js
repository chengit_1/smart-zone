const energyApiUrl = "http://localhost:8080"

// 能源系统汇总数据
const energySummary = "/unicom/energy/summary"
// 能源系统报表数据
const energyChart = "/unicom/energy/chart"
// 能源系统页脚数据
const energyFooter = "/unicom/energy/footer"


