//axios封装post请求
function axiosPostRequst(url,data) {
    let result = axios({
        method: 'post',
        url: url,
        data: data,
        transformRequest:[function(data){
            let ret = '';
            for(let i in data){
                ret += encodeURIComponent(i)+'='+encodeURIComponent(data[i])+"&";
            }
            return ret;
        }],
        header:{
            'Content-type': 'application/x-www-form-urlencoded' // 默认值
        }
    }).then(resp=> {
        return resp.data;
    }).catch(error=>{
        return "exception="+error;
    });
    return result;
}

//get请求
function axiosGetRequst(url,data) {
    var result = axios({
        method: 'get',
        url: url,
        params: data
    }).then(function (resp) {
        return resp.data;
    }).catch(function (error) {
        return "exception=" + error;
    });
    return result;
}