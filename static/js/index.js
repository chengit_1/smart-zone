$(function() {
	var options = {
		speed: 2000,//匀速时间
		direction: 'vertical',
		autoplay: {
			delay: 0,
			stopOnLastSlide: false,
			disableOnInteraction: false,
		},
		loop: true,
		loopedSlides: 5,
		slidesPerView: 5,
		spaceBetween: 0,
		observer: true,
		observeParents: true, // 当Swiper的父元素变化时，会更新swiper
	}

	$.extend({
		"swiperOption":function(f1, f2){
			let swiper = new Swiper(f1, f2);  
			return swiper;               
		},
	});
	
	var swiper0 = $.swiperOption($('.famous_school_item_swiper').eq(0).find('.swiper-container'), options);  // 案例一
	// var swiper1 = $.swiperOption($('.famous_school_item_swiper').eq(1).find('.swiper-container'), options);  // 案例二
	// var swiper2 = $.swiperOption($('.famous_school_item_swiper').eq(2).find('.swiper-container'), options);  // 案例三
	
	
	// swiper1.autoplay.stop();
	// swiper2.autoplay.stop();
	
	new Swiper('#famous_school_list', {
		spaceBetween: 0,
		slidesPerView: 'auto',//一行显示3个
		observer: true,
		observeParents: true, // 当Swiper的父元素变化时，会更新swiper
		paginationClickable: true,
		pagination: {
			el: '.swiper-pagination.two',
			clickable: true
		},
		on: {
			// transitionEnd: function(swiper) {
			// 	// console.log(this.snapIndex)
			// 	let snapIndex = this.snapIndex
			// 	if (snapIndex == 0) {
			// 		swiper1.autoplay.stop();
			// 		swiper2.autoplay.stop();
			// 		swiper0.autoplay.start();
			// 		swiper0.init();
			// 		swiper0.update();
			// 	} else if (snapIndex == 1) {
			// 		swiper0.autoplay.stop();
			// 		swiper2.autoplay.stop();
			// 		swiper1.autoplay.start();
			// 		swiper1.init();
			// 		swiper1.update();
			// 	} else if (snapIndex == 2) {
			// 		swiper0.autoplay.stop();
			// 		swiper1.autoplay.stop();
			// 		swiper2.autoplay.start();
			// 		swiper2.init();
			// 		swiper2.update();
			// 	}
			// }
		}
	})
})